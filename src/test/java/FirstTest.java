import org.junit.Assert;
import org.junit.Test;

public class FirstTest {

    @Test
    public void getSomeTextTest(){
        First first = new First();
        String s = first.getSomeText();
        Assert.assertEquals("Some text", s);
    }

//    @Test
//    public void getWrongTextTest(){
//        First first = new First();
//        String s = first.getSomeText();
//        Assert.assertEquals("Some text two", s);
//    }
}
